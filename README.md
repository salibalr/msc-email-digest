# MSC Email Digest

A script for Google Sheets to consume responses to a Google Form and aggregate them into an HTML email.

## Author
Lauren Saliba - [github](http://github.com/salibalr)

