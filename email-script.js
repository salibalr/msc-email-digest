function onOpen(e) {
	SpreadsheetApp.getUi()
	.createMenu('Emails')
	.addItem('Generate Manual Email', 'createEmail')
	.addToUi();
}

function isEmpty(str) {
	return (!str || 0 === str.length);
}

function createEmail() {

	var ss = SpreadsheetApp.getActiveSpreadsheet();
	ss.setActiveSheet(ss.getSheetByName("Form Responses 1"));

	var sheet = SpreadsheetApp.getActiveSheet();

	var dataRange = sheet.getDataRange();
	var data = dataRange.getValues();

	// create list of categories
	var categoryList = ["Academic", "Admissions", "Athletics/Wellness", "Dining", "Employee Announcements", "Events", "Facilities", "MAC", "Misc"];

	// declare variables
	var messageContent = [];

	var stuCategories = {};
	var facCategories = {};
	var allCategories = {};
	for(var i = 0; i< categoryList.length; i++){
		stuCategories[i] = [];
		facCategories[i] = [];
		allCategories[i] = [];
	}

	// iterate through data
	for(var i = 0; i<data.length;i++){

		function formatData() {
			var rowData = data[i];
			var title = rowData[1];
			var messageData = rowData[2];
			var message = messageData.replace(/\n/g, "</p><p>");
			var linkData = rowData[3];
			var category = rowData[5];
			var dateValue = rowData[7];

			if(isEmpty(dateValue)){
				var date = '';
			} else {
				var date = Utilities.formatDate(new Date(dateValue), Session.getScriptTimeZone(), "MMM. d");	
			}

			var timeValue = rowData[8];

			if(isEmpty(timeValue)){
				var time = '';
			} else {

				if(isEmpty(date)){
					var timeStr = Utilities.formatDate(new Date(timeValue), Session.getScriptTimeZone(), "h:mm a");
				} else {
					var timeStr = ' at ' + Utilities.formatDate(new Date(timeValue), Session.getScriptTimeZone(), "h:mm a");
				}
				var time = timeStr.replace("AM", "a.m.").replace("PM", "p.m.").replace("12:00 p.m.", "noon").replace("12:00 a.m.", "midnight");

			}

			var locationValue = rowData[9];

			if(isEmpty(locationValue)){
				var location = '';
			} else {

				if((isEmpty(date)) && (isEmpty(time))){
					var location = locationValue;
				} else {
					var location =  ' &bull; ' + locationValue;
				}

			}

			var name = rowData[10] + ' ' +  rowData[11];
			var emailAddress = rowData[12];
			var phone = rowData[13];

			var messageOpen = '<tr><td bgcolor=\"#ffffff\" style=\"padding:0 0 10px 0\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"550\" style=\"font-family:Arial;font-size:10pt;line-height:150%;\">';
            var messageTitle = '<tr><td style=\"padding:10px 0 5px 0\"><h3 style=\"font-size:150%;color:#00502F;margin:0 0 0 0;line-height:100%;\">' + title + '</h3></td></tr>';
			if((isEmpty(date)) && (isEmpty(time)) && (isEmpty(location))){
				var dateTimeLoc = '';
			} else {
              var dateTimeLoc = '<tr><td style=\"padding:0 0 0 0;\"><h4 style=\"font-size:115%;color:#00966C;margin:0;line-height:100%;\">' + date + time + location + '</h4></td></tr>';
			};
            var messageBody = '<tr><td style=\"padding:20px 0 10px 0;text-align:justify;line-height:145%;\"><p>' + message + '</p></td></tr>';
            var contactBlock = '<tr><td style=\"padding:10px 0 10px 0;line-height:105%;\"><h4 style=\"font-size:125%;color:#00966C;margin:0;\">Contact</h4><p style=\"margin-top:0;\">' + name + '<br>' + emailAddress + '<br>' + phone + '</p></td></tr>';
			if(isEmpty(linkData)){
				var link = '';
			} else  {
				var link = '<tr><td style=\"padding: 0 0 10px 0\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"font-family:Arial;font-size:10pt;line-height:100%;\"><tr><td width=\"160\"></td><td width=\"160\"></td><td width=\"175\" bgcolor=\"#00502F\" style=\"padding:5px 25px 5px 25px;text-align:center;\"><a style=\"text-transform:uppercase;color:#ffffff;text-decoration:none;font-weight:bold;letter-spacing:1px;\" href=\"' + linkData + '\">Learn More &raquo;</a></td></tr></table></td></tr>';
			}
			var messageClose = '<tr><td style=\"border-top:1px solid #F4803E;\" height=\"10\"></td></tr></table></td></tr>';

			messageContent = messageOpen + messageTitle + dateTimeLoc + messageBody + contactBlock + link + messageClose;
		}

		var editionCount = data[i][6];
		var audience = data[i][4];
		var category = data[i][5];

		if(editionCount !== 0){

			// locate Student-only content
			if(audience == "Students"){
				var stuCat = [];

				for(var j = 0; j < categoryList.length; j++){

					if(category == categoryList[j]) {
						formatData();
						stuCat[j] = messageContent;
					}
				}

				for(var k = 0; k < Object.keys(stuCategories).length; k++) {
					stuCategories[k] = stuCategories[k].concat(stuCat[k]);
				}
			}

			// locate Faculty-only content
			if(audience == "Faculty/Staff"){
				var facCat = [];

				for(var j = 0; j < categoryList.length; j++){

					if(category == categoryList[j]) {
						formatData();
						facCat[j] = messageContent;
					}
				}

				for(var k = 0; k < Object.keys(stuCategories).length; k++) {
					facCategories[k] = facCategories[k].concat(facCat[k]);
				}
			}

			// locate All Audiences Content
			if(audience == "Faculty/Staff, Students"){

				var allCat = [];

				for(var j = 0; j < categoryList.length; j++){

					if(category == categoryList[j]) {
						formatData();
						allCat[j] = messageContent;
					}
				}

				for(var k = 0; k < Object.keys(stuCategories).length; k++) {
					allCategories[k] = allCategories[k].concat(allCat[k]);
				}
			}

			// increment editionValue
			if(i > 0) {
				var cellNumber = i + 1;
				var rangeValue = "G" + cellNumber;
				var cell = sheet.getRange(rangeValue);
				var cellValue = cell.getValue();

				cell.setValue(cellValue-1);
			}
		} 
	}

	// create category headers and no announcements text
	var categoryOpen = '<tr><td style=\"padding:0 0 0 0;\"><table align=\"center\" bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"font-family:Arial;font-size:10pt;line-height:150%;\">';
    var headerOpen = '<tr><td style=\"padding:0 0 0 0\"><table align=\"center\" bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"font-family:Arial;font-size:10pt;line-height:150%;\"><tr><td width=\"25\"></td><td width=\"549\" bgcolor=\"#00966C\" style=\"padding:5px 10px 5px 10px;\"><h2 style=\"font-size:110%;color:#ffffff;text-transform:uppercase;margin:0 0 0 0;letter-spacing:1px;line-height:100%;\">';
	var headerClose = '</h2></td><td width=\"25\"></td></td></tr></table></td></tr>';
	var categoryClose = '</table></td></tr>';

	// check if categories are empty; if not, print headers
	var stuContent = [];
	var stuMessageContent = [];
	for (var i = 0; i < categoryList.length; i++) {
		if((isEmpty(stuCategories[i].join(""))) && (isEmpty(allCategories[i].join("")))) {
			stuContent[i] = "";
		} else {
			stuContent[i] = categoryOpen + headerOpen + categoryList[i] + headerClose + allCategories[i].join("") + stuCategories[i].join("") + categoryClose;
		}
	}
	stuMessageContent = stuMessageContent.concat(stuContent);

	var facContent = [];
	var facMessageContent = [];
	for (var i = 0; i < categoryList.length; i++) {
		if((isEmpty(facCategories[i].join(""))) && (isEmpty(allCategories[i].join("")))) {
			facContent[i] = "";
		} else {
			facContent[i] = categoryOpen + headerOpen + categoryList[i] + headerClose + allCategories[i].join("") + facCategories[i].join("") + categoryClose;
		}
	}
	facMessageContent = facMessageContent.concat(facContent);	

	// format emails
	var logoID = 'http://documents.morrisville.edu/images/logo_new.png';
	var logoIDBlob = UrlFetchApp.fetch(logoID).getBlob().setName("logoIDBlob");
	var facebookID = 'http://www.morrisville.edu/images/smi-facebook.png';
	var facebookIDBlob = UrlFetchApp.fetch(facebookID).getBlob().setName("facebookIDBlob");
	var twitterID = 'http://www.morrisville.edu/images/smi-twitter.png';
	var twitterIDBlob = UrlFetchApp.fetch(twitterID).getBlob().setName("twitterIDBlob");
	var youtubeID = 'http://www.morrisville.edu/images/smi-youtube.png';
	var youtubeIDBlob = UrlFetchApp.fetch(youtubeID).getBlob().setName("youtubeIDBlob");
	var flickrID = 'http://www.morrisville.edu/images/smi-flickr.png';
	var flickrIDBlob = UrlFetchApp.fetch(flickrID).getBlob().setName("flickrIDBlob");
	var instagramID = 'http://www.morrisville.edu/images/smi-instagram.png';
	var instagramIDBlob = UrlFetchApp.fetch(instagramID).getBlob().setName("instagramIDBlob");

	var todayClean = Utilities.formatDate(new Date(), Session.getScriptTimeZone(), "MMM. d, yyyy");
  
    var featureSection = HtmlService.createHtmlOutputFromFile('feature').getContent();

	var emailHeader = HtmlService.createHtmlOutputFromFile('header-top').getContent() + todayClean + HtmlService.createHtmlOutputFromFile('header-bottom').getContent();  
	var emailFooter = HtmlService.createHtmlOutputFromFile('footer').getContent();
  
    var facSubFooter = HtmlService.createHtmlOutputFromFile('fac-subfooter').getContent();
    var emailClose = '</table></body>';

	var studentDigestMessage = emailHeader + featureSection + stuMessageContent.join("") + emailFooter + emailClose;
	var facDigestMessage = emailHeader + featureSection + facMessageContent.join("") + emailFooter + facSubFooter + emailClose;

	//send emails
	MailApp.sendEmail({
		to: 'commmark@morrisville.edu',
		cc: 'web@morrisville.edu',
		subject: 'Student Email Digest for ' + todayClean,
		htmlBody: studentDigestMessage,
		inlineImages: {
			logoID: logoIDBlob,
			facebookID: facebookIDBlob,
			twitterID: twitterIDBlob,
			youtubeID: youtubeIDBlob,
			flickrID: flickrIDBlob,
			instagramID: instagramIDBlob
		}
	});

	MailApp.sendEmail({
		to: 'commmark@morrisville.edu',
		cc: 'web@morrisville.edu', 
		subject: 'Faculty/Staff Email Digest for ' + todayClean,
		htmlBody: facDigestMessage,
		inlineImages: {
			logoID: logoIDBlob,
			facebookID: facebookIDBlob,
			twitterID: twitterIDBlob,
			youtubeID: youtubeIDBlob,
			flickrID: flickrIDBlob,
			instagramID: instagramIDBlob
		}
	});
}
